# API SPEC

## Authentication
All API must use this authentication

Request:
- Header:
    - X-Api-Key: "your-api-key"

##  Create Product API
Request:
- Method: POST
- Endpoint: `/api/products`
- Headers:
```json
{
    "Content-Type": "application/json"
}
```
- Body:
```json
{
    "id": "string, unique",
    "name": "Product 1",
    "price": 1000,
    "description": "Product 1 description"
}
```
- Response:
```json
{
    "id": "1",
    "name": "Product 1",
    "price": 1000,
    "description": "Product 1 description",
    "createdAt": "2021-08-10T07:00:00.000Z"
}
```

## Get Product
Request:
- Method: GET
- Endpoint: `/api/products/:id`
- Header:
```json
{
    "Content-Type": "application/json"
}
```
- Response:
```json
{
    "id": "1",
    "name": "Product 1",
    "price": 1000,
    "description": "Product 1 description",
    "createdAt": "2021-08-10T07:00:00.000Z"
}
```

## Update Product
Request:
- Method: PUT
- Endpoint: `/api/products/:id`
- Body:
```json
{
    "id": 1,
    "name": "Product 1",
    "price": 1000,
    "description": "Product 1 description"
}
```
Response:
```json
{
    "id": "1",
    "name": "Product 1",
    "price": 1000,
    "description": "Product 1 description",
    "createdAt": "2021-08-10T07:00:00.000Z",
    "updatedAt": "2021-08-10T07:00:00.000Z"
}
```

## List Product
Request:
- Method: GET
- Endpoint: `/api/products`
- Header:
    - Accept: application/json
- Query Param:
    - page: number
    - limit: number
    - sort: string
    - order: string
    - search: string
    - filter: string
    - size: number


Response: 
``` json
{
    "code": 200,
    "status": "success",
    "message": "Products retrieved successfully",
    "data": [
        {
            "id": "1",
            "name": "Product 1",
            "price": 1000,
            "description": "Product 1 description",
            "createdAt": "2021-08-10T07:00:00.000Z",
            "updatedAt": "2021-08-10T07:00:00.000Z"
        },
        {
            "id": "2",
            "name": "Product 2",
            "price": 2000,
            "description": "Product 2 description",
            "createdAt": "2021-08-10T07:00:00.000Z",
            "updatedAt": "2021-08-10T07:00:00.000Z"
        }
    ]
}
```
## Delete Product
Request:
- Method: DELETE
- Endpoint: `/api/products/:id`
- Header:
```json
{
    "Content-Type": "application/json"
}
```
Response:
```json
{
    "code": 200,
    "status": "success",
    "message": "Product deleted successfully"
}

