package telkomindonesia.kotlin.restful.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import telkomindonesia.kotlin.restful.api.entity.ApiKey

interface ApiKeyRepository : JpaRepository<ApiKey, String> {
}