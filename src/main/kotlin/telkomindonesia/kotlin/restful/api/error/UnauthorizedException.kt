package telkomindonesia.kotlin.restful.api.error

import java.lang.Exception

class UnauthorizedException : Exception() {
}