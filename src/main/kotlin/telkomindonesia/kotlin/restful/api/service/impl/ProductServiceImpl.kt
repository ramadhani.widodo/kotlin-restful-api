package telkomindonesia.kotlin.restful.api.service.impl

import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import telkomindonesia.kotlin.restful.api.entity.Product
import telkomindonesia.kotlin.restful.api.error.NotFoundException
import telkomindonesia.kotlin.restful.api.model.CreateProductRequest
import telkomindonesia.kotlin.restful.api.model.ListProductRequest
import telkomindonesia.kotlin.restful.api.model.ProductResponse
import telkomindonesia.kotlin.restful.api.model.UpdateProductRequest
import telkomindonesia.kotlin.restful.api.repository.ProductRepository
import telkomindonesia.kotlin.restful.api.service.ProductService
import telkomindonesia.kotlin.restful.api.validation.ValidationUtil
import java.util.*
import java.util.stream.Collectors

@Service
class ProductServiceImpl(
    val productRepository: ProductRepository,
    private val validationUtil: ValidationUtil
): ProductService {

    override fun create(createProductRequest: CreateProductRequest): ProductResponse {
        validationUtil.validate(createProductRequest)

        val product = Product (
            id = createProductRequest.id!!,
            name = createProductRequest.name!!,
            price = createProductRequest.price!!,
            description = createProductRequest.description!!,
            quantity = createProductRequest.quantity!!,
            createdAt = Date(),
            updatedAt = null
        )

        productRepository.save(product)

        return convertProductToProductResponse(product)
    }

    override fun get(id: String): ProductResponse {
        val product = findOrThrow(id)

        return convertProductToProductResponse(product)
    }

    override fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse {
        val product = findOrThrow(id)

        validationUtil.validate(updateProductRequest)

        product.apply {
            name = updateProductRequest.name!!
            price = updateProductRequest.price!!
            description = updateProductRequest.description!!
            quantity = updateProductRequest.quantity!!
            updatedAt = Date()
        }

        productRepository.save(product)

        return convertProductToProductResponse(product)
    }

    override fun delete(id: String) {
        val product = findOrThrow(id)

        productRepository.delete(product)
    }

    override fun list(listProductRequest: ListProductRequest): List<ProductResponse> {
        val page = productRepository.findAll(PageRequest.of(listProductRequest.page, listProductRequest.size))
        val products: List<Product> = page.get().collect(Collectors.toList())
        return products.map { convertProductToProductResponse(it) }
    }

    private fun findOrThrow(id: String): Product {
        val product = productRepository.findByIdOrNull(id)

        if (product == null) {
            throw NotFoundException()
        } else {
            return product
        }
    }

    private fun convertProductToProductResponse (product: Product) : ProductResponse {
        return ProductResponse(
            id = product.id,
            name = product.name,
            price = product.price,
            description = product.description,
            quantity = product.quantity,
            createdAt = product.createdAt,
            updatedAt = product.updatedAt
        )
    }
}