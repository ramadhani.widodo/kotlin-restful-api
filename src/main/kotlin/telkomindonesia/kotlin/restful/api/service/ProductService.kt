package telkomindonesia.kotlin.restful.api.service

import telkomindonesia.kotlin.restful.api.model.CreateProductRequest
import telkomindonesia.kotlin.restful.api.model.ListProductRequest
import telkomindonesia.kotlin.restful.api.model.ProductResponse
import telkomindonesia.kotlin.restful.api.model.UpdateProductRequest

interface ProductService {
    fun create(createProductRequest: CreateProductRequest): ProductResponse

    fun get(id: String): ProductResponse

    fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse

    fun delete(id: String)

    fun list(listProductRequest: ListProductRequest): List<ProductResponse>
}
