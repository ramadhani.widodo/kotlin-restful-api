package telkomindonesia.kotlin.restful.api.config

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import telkomindonesia.kotlin.restful.api.entity.ApiKey
import telkomindonesia.kotlin.restful.api.repository.ApiKeyRepository

@Component
class ApiKeySeeder (val apiKeyRepository: ApiKeyRepository) : ApplicationRunner {

    val apiKey = "SECRET"

    override fun run(args: ApplicationArguments?) {
        if (!apiKeyRepository.existsById(apiKey)) {
            val entity = ApiKey(apiKey)
            apiKeyRepository.save(entity)
        }
    }
}