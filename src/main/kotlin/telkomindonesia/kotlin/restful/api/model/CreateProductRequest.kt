package telkomindonesia.kotlin.restful.api.model

import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.util.*

data class CreateProductRequest (
    @field:NotBlank
    val id: String?,

    @field:NotBlank
    val name: String?,

    @field:NotNull
    @field:Min(1)
    val price: Long?,

    val description: String?,

    @field:NotNull
    @field:Min(0)
    val quantity: Int?,

    val createdAt: Date?,

    val updatedAt: Date?
)