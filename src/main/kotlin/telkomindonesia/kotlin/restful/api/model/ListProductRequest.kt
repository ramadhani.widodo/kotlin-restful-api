package telkomindonesia.kotlin.restful.api.model

data class ListProductRequest(
    val page: Int,

    val size: Int
)
